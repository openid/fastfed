   This folder contains documents describing the user experiences which FastFed seeks to enable, plus detailed examples of the message flows for implementing specific scenarios using profiles such as SAML and SCIM.

   Start by reading the "Overview of FastFed Scenarios". The initial priority of the working group is solving Scenario #1.